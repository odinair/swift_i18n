swift_i18n
==========

swift_i18n is a simple yet intelligent translator powered by `pyseeyou`_ and `Babel`_.

.. toctree::
  api

.. _pyseeyou: https://github.com/rolepoint/pyseeyou
.. _Babel: http://babel.pocoo.org/en/latest/

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
