from swift_i18n import Translator

# Optional best practice:
import logging
from swift_i18n.util import log

log.addHandler(logging.StreamHandler())
log.setLevel(logging.WARNING)

# This Translator will load toml files from ./locales/ when we request translated strings.
translate = Translator(__file__)

print(translate("hello_world"))  # 'Hello, world'

# Formatting strings is super simple:
print(translate("friend_request", name="Emma"))  # 'Emma sent you a friend request.'
# Do note however that if a string requires formatting arguments, you *must* pass them when
# calling your translator instance, otherwise a KeyError will be raised.

# Pluralization is also cleanly handled:
print(translate("messages", count=0))  # 'You have no unread messages'
print(translate("messages", count=1))  # 'You have 1 unread message'
print(translate("messages", count=2))  # 'You have 2 unread messages'

# Strings can also be gendered without having to add multiple variations of the same string:

# 'James invited you to join his group.'
print(translate("group_invite", name="James", gender="male"))
# 'Emily invited you to join her group.'
print(translate("group_invite", name="Emily", gender="female"))
# 'Blake invited you to join their group.'
print(translate("group_invite", name="Blake", gender="other"))
