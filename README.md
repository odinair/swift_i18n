# swift_i18n

Simple yet intelligent translator based on ICU message formatting.

This project is currently in an alpha state, and as such the public API may change in breaking ways at any time (but *probably* won't).

This is primarily designed to be used as a framework to expand upon, depending on your needs. For an example use of this library,
see my [swift_libs](https://gitlab.com/odinair/swift_libs/blob/master/swift_libs/i18n.py) library, which is used in conjunction
with my Red cogs.

## Setup

This project requires at least Python 3.7 to function.

Until I can be bothered to publish this on PyPI (likely after I come up with a better name for this project),
you'll have to tell pip to use my index to be able to install this package, like so:

```sh
$ pip install --extra-index-url=https://py.odinair.xyz/ swift_i18n
```

A simple example usage of this project is included in [examples/example.py](https://gitlab.com/odinair/swift_i18n/blob/master/examples/example.py).
